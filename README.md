# flipstarters

Hosted on https://flipstarters.bitcoincash.network

## Add a project

To request a project to be added to the overview, submit an issue or a PR modifying `src/flipstarters.json`.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
